package facci.pm.realmdb;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

public class Task extends RealmObject {
    @Required
    private String categoria;
    @Required
    private String talla;
    @Required
    private String precio;
    @Required
    private String marca;

    @Required
    private String status = TaskStatus.Open.name();

    public Task(String _categoria, String _talla, String _precio,  String _marca)
    { this.categoria= _categoria; this.talla = _talla; this.precio = _precio; this.marca = _marca;}
    public Task() {}

    public void setStatus(TaskStatus status) { this.status = status.name(); }
    public String getStatus() { return this.status; }

    public String getCategoria() { return categoria; }
    public void setCategoria(String categoria) { this.categoria = categoria; }

    public String getTalla() { return talla; }
    public void setTalla(String talla) { this.talla = talla; }

    public String getPrecio() { return precio; }
    public void setPrecio(String precio) { this.precio = precio; }

    public String getMarca() { return marca; }
    public void setMarca(String marca) { this.marca = marca; }


}
